/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
  TextInput,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  const [count, setCount] = useState(0);
  const [text, setText] = useState('example@example.com');
  const [onModal, setOnModal] = useState(false);
  const increment = () => {
    setCount(() => count + 1);
  };
  const decrement = () => {
    setCount(() => count - 1);
  };
  const onChangeText = text => {
    setText(text);
  };
  console.log(text);
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View
            style={{
              flex: 6,
              justifyContent: 'center',
              alignItems: 'center',
              height: 300,
            }}>
            <Text style={{color: Colors.primary}}>count is {count}</Text>
            <Image
              style={{width: 50, height: 50}}
              source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
            />
            <Button title="incrementar" onPress={() => increment()} />
            <Button title="decremetar" onPress={() => decrement()} />
            <Text style={{color: Colors.primary}}>your email: {text}</Text>
            <TextInput
              style={{
                height: 40,
                width: 150,
                borderColor: 'gray',
                borderWidth: 1,
              }}
              placeholder="email"
              onChangeText={text => onChangeText(text)}
              value={text}
            />
            <Modal
              animationType="slide"
              transparent={false}
              visible={onModal}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <View style={{marginTop: 22}}>
                <View>
                  <Text>Modal is visible</Text>
                  <Image
                    style={{width: 50, height: 50}}
                    source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
                  />
                  <TouchableHighlight
                    onPress={() => {
                      setOnModal(!onModal);
                    }}>
                    <Text>Hide Modal</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </Modal>
            <Button title="view Modal" onPress={() => setOnModal(!onModal)} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.dark,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
});

export default App;
